const $ = require('jquery');
global.$ = global.jQuery = $;
import('bootstrap/js/dist/modal');


function setFormData(formType) {

  let header;
  let placeholder;

  switch (formType) {
    case "speaker":
      header = 'Become a Speaker';
      placeholder = 'Why you would like to join the ECOMMTALKS community?*';
      break;
    case "sponsor":
      header = 'BECOME A SPONSOR';
      placeholder = 'Message*';
      break;
    case "admission":
      header = 'Apply for admission';
      placeholder = 'Why you would like to join the ECOMMTALKS community?*';
      break;
  }

  $('input[name="type"]').val(formType);
  $('.modal .modal-header h1').html(header);
  $('.modal textarea').attr('placeholder', placeholder);
  $('.modal-footer button').text();
}

$('.modal-button').on('click', function () {
  const type = $(this).data('type');
  setFormData(type);
  $('.modal').modal('show');
  $('.form_field.fullName').trigger('focus')
});

$('#becomeFormSubmit').click(() => {
 submitModalForm();
});

$('button[form="contact_speaker"]').click(() => {
  submitModalForm(true);
});

$(".modal").on("hidden.bs.modal", function () {
  const form = $(this).find('form');

  form.removeClass('hidden');
  form[0].reset();
  $('.modal-wrap').addClass('hidden');
  $('.text-danger').html('');
  $('.has-error').removeClass('has-error');
});

function submitModalForm(speaker = false) {
  const form = $('.modal-form');
  const formData = new FormData(form[0]);

  const url = speaker ? `/contact/speaker` : `/contact/form`;

  $.ajax({
    url: url,
    type: 'POST',
    cache: false,
    contentType: false,
    processData: false,
    data: formData,
    success: () => {
      form.addClass('hidden');
      $('.modal .modal-header h1').html("Success");
      $('.modal-wrap').removeClass('hidden');
      form[0].reset();
      $('.text-danger').html('');
      $('.has-error').removeClass('has-error')
    },
    error: ({responseJSON}) => {
      $('.text-danger').html('');
      $('.has-error').removeClass('has-error');
      const {violations} = responseJSON
      violations.forEach(violation => {
        const fieldName = violation.propertyPath.replace('data.', '');
        if (violation.propertyPath !== 'data.message') {
          const field = ".form_field." + fieldName;
          $(field).parent().addClass('has-error');
          $(field).parent().prev().html(violation.title.replace('This value', capitalizeFirstLetter(fieldName)));
          return false;
        } else {
          const field = ".form_textfield." + fieldName;
          $(field).parent().addClass('has-error');
          $(field).parent().prev().html(violation.title.replace('This value', capitalizeFirstLetter(fieldName)));
          return false;
        }
      })
    },
  })
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

$("[id^=author_bio]").each(function () {
  const maxChars = $(this).attr('id').includes("author_bio_") ? 500 : 250;
  const $fullElem = $(this).find(".text.full");
  const $readMoreButton = $(this).find(".read-more");
  const text = $fullElem.text();
  const html = $fullElem.html();

  let $readMoreText = null;
  let $ellipsis = null;

  if (text.length > maxChars) {
    const splitIndex = maxChars - 3;
    const updatedHtml = html.substring(0, splitIndex) + "<span class='ellipsis'>...</span><span id='read-more'>" + html.substring(splitIndex) + "</span>";
    $readMoreButton.css('display', 'inline-block');
    $fullElem.html(updatedHtml);
    $readMoreText = $(this).find("#read-more");
    $ellipsis = $(this).find(".ellipsis");
    $readMoreText.hide();
  }

  $fullElem.show();

  $readMoreButton.click(function () {
    if ($readMoreText.is(":visible")) {
      $readMoreText.slideUp(200);
      $ellipsis.show();
      $(this).text("Read More");
    } else {
      $ellipsis.hide();
      $readMoreText.slideDown(200);
      $(this).text("Read Less");
    }
  });
});



