<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210217112215 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO ecommtalks.home_config (header_title, header_description, left_column, right_column) VALUES ('<div>Premium Content for Private Community</div>', '<div>ECOMMTALKS conference presents online Academy - the series of educational and motivational speeches from the IT industry.</div>', '<ul><li>Hot Topics&nbsp;</li><li>Deep Insides&nbsp;</li><li>10+ Hours&nbsp;</li><li>Top Experts&nbsp;</li><li>Global Trends&nbsp;</li><li>Panel Discussions</li></ul>', '<ul><li>Future</li><li>Fintech</li><li>Growth</li><li>Regtech</li><li>Ecommerce</li><li>Expansion</li></ul>');");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('TRUNCATE TABLE ecommtalks.home_config;');
    }
}
