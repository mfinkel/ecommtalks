<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201027165158 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'ALTER TABLE video ADD is_display_on_home_page TINYINT(1) NOT NULL, DROP is_display_home_page_one, DROP is_display_home_page_two'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'ALTER TABLE video ADD is_display_home_page_two TINYINT(1) NOT NULL, CHANGE is_display_on_home_page is_display_home_page_one TINYINT(1) NOT NULL'
        );
    }
}
