<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201025181513 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'ALTER TABLE course ADD is_active TINYINT(1) NOT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE speaker ADD is_active TINYINT(1) NOT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE user ADD registered_code VARCHAR(24) NOT NULL, ADD is_active TINYINT(1) NOT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE video ADD is_active TINYINT(1) NOT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE course DROP is_active, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE speaker DROP is_active, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE user DROP registered_code, DROP is_active, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE video DROP is_active, DROP created_at, DROP updated_at');
    }
}
