<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201112115448 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }
    
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2C762690C1');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2C952717D1');
        $this->addSql('DROP TABLE video_file');
        $this->addSql('DROP TABLE video_image');
        $this->addSql('DROP INDEX UNIQ_7CC7DA2C952717D1 ON video');
        $this->addSql('DROP INDEX UNIQ_7CC7DA2C762690C1 ON video');
        $this->addSql(
            'ALTER TABLE video ADD image_name VARCHAR(255) NOT NULL, ADD image_size INT NOT NULL, DROP video_file_id, DROP video_image_id'
        );
    }
    
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'CREATE TABLE video_file (id INT AUTO_INCREMENT NOT NULL, video VARCHAR(200) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, mime_type VARCHAR(32) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, size VARCHAR(32) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' '
        );
        $this->addSql(
            'CREATE TABLE video_image (id INT AUTO_INCREMENT NOT NULL, image VARCHAR(200) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, mime_type VARCHAR(32) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, size VARCHAR(32) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' '
        );
        $this->addSql(
            'ALTER TABLE video ADD video_file_id INT DEFAULT NULL, ADD video_image_id INT DEFAULT NULL, DROP image_name, DROP image_size'
        );
        $this->addSql(
            'ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2C762690C1 FOREIGN KEY (video_file_id) REFERENCES video_file (id)'
        );
        $this->addSql(
            'ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2C952717D1 FOREIGN KEY (video_image_id) REFERENCES video_image (id)'
        );
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7CC7DA2C952717D1 ON video (video_image_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7CC7DA2C762690C1 ON video (video_file_id)');
    }
}
