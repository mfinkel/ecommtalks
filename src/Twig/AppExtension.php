<?php

namespace App\Twig;

use App\Entity\Course;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getSectionsList', [$this, 'getSectionsList']),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('lengthToTime', [$this, 'getHoursMinutes']),
        ];
    }

    public function formatPrice($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }

    public function getHoursMinutes($seconds, $format = 'H:i:s')
    {
        if (empty($seconds) || !is_numeric($seconds)) {
            return false;
        }

        return gmdate($format, $seconds);
    }

    /**
     * @return Course[]
     */
    public function getSectionsList(): iterable
    {
        return $this->manager->getRepository(Course::class)->getCoursesList();
    }
}
