<?php

namespace App\Service;

use Symfony\Component\Serializer\SerializerInterface;

class CsvExporter
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
    
    private function getColumns(array $data)
    {
        return $this->serializer->normalize(
            $data,
            'csv',
            [
                'groups' => [
                    'export',
                ],
            ]
        );
    }

    public function exportRegistrationToCsv(array $registrationCodes)
    {
        $columns = $this->getColumns($registrationCodes);
        $output = fopen('php://output', 'w') or die("Can't open php://output");
        header('Content-Type:application/csv');
        header('Content-Disposition:attachment;filename=registration_codes.csv');
        fputcsv($output, ['Fist Name', 'Last Name', 'Email', 'Registration Code']);
        foreach ($columns as $product) {
            fputcsv($output, $product);
        }
        fclose($output) or die("Can't close php://output");
        exit;
    }
    
    public function exportUsersToCsv(array $users)
    {
        $columns = $this->getColumns($users);
        $output = fopen('php://output', 'w') or die("Can't open php://output");
        header('Content-Type:application/csv');
        header('Content-Disposition:attachment;filename=users.csv');
        fputcsv($output, ['Fist Name', 'Last Name', 'Email', 'Registration Code', 'isActive', 'createdAt', 'facebook Id', 'google Id']);
        
        foreach ($columns as $product) {
            fputcsv($output, $product);
        }
        fclose($output) or die("Can't close php://output");
        exit;
    }
}
