<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;

class MailerService
{
    /**
     * @var Mailer
     */
    private $mailer;
    
    /**
     * @var string|null
     */
    private $copyTo;
    /**
     * @var string|null
     */
    private $replyTo;
    /**
     * @var string
     */
    private $from;
    /**
     * @var string
     */
    private $subject;
    
    public function __construct(Mailer $mailer, string $from, string $subject, ?string $copyTo, ?string $replyTo)
    {
        $this->mailer = $mailer;
        $this->from = $from;
        $this->subject = $subject;
        $this->copyTo = $copyTo;
        $this->replyTo = $replyTo;
    }
    
    /**
     * @param User $user
     * @throws TransportExceptionInterface
     */
    public function sendConfirmationEmail(User $user): void
    {
        $email = (new TemplatedEmail())
            ->from($this->from)
            ->to($user->getEmail())
            ->subject($this->subject)
            ->htmlTemplate('emails/registration.html.twig')
            ->context(
                [
                    'user' => $user,
                ]
            );
        
        $this->mailer->send($email);
    }
    
    /**
     * @param string $sendTo
     * @param string $sendToSubject
     * @param string $fromName
     * @param string $fromEmail
     * @param string $fromCompany
     * @param string $fromPosition
     * @param string $fromMessage
     * @throws TransportExceptionInterface
     */
    public function sendContactFormEmail(
        string $sendTo,
        string $sendToSubject,
        string $fromName,
        string $fromEmail,
        string $fromCompany,
        string $fromPosition,
        string $fromMessage
    ): void {
        $email = (new TemplatedEmail())
            ->from($this->from)
            ->replyTo($fromEmail)
            ->to($sendTo)
            ->subject($sendToSubject)
            ->htmlTemplate('emails/contact_form.html.twig')
            ->context(
                [
                    'sendToSubject' => $sendToSubject,
                    'sendTo' => $sendTo,
                    'fromName' => $fromName,
                    'fromEmail' => $fromEmail,
                    'fromCompany' => $fromCompany,
                    'fromPosition' => $fromPosition,
                    'fromMessage' => $fromMessage,
                ]
            );
        
        $this->mailer->send($email);
    }

    /**
     * @param string $speaker
     * @param string $sendTo
     * @param string $subject
     * @param string $fromName
     * @param string $fromEmail
     * @param string $fromMessage
     * @throws TransportExceptionInterface
     */
    public function sendContactSpeakerFormEmail(
        string $speaker,
        string $sendTo,
        string $subject,
        string $fromName,
        string $fromEmail,
        string $fromMessage
    ): void {
        $email = (new TemplatedEmail())
            ->from($this->from)
            ->replyTo($fromEmail)
            ->to($sendTo)
            ->subject($subject)
            ->htmlTemplate('emails/contact_speaker_form.html.twig')
            ->context(
                [
                    'speaker' => $speaker,
                    'fromName' => $fromName,
                    'fromEmail' => $fromEmail,
                    'fromMessage' => $fromMessage,
                ]
            );

        $this->mailer->send($email);
    }
}
