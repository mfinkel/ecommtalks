<?php

namespace App\Service;

class KeyGen
{
    public function generate(int $start = 0, int $length = 20, int $split = 4)
    {
        return implode(
            '-',
            str_split(
                substr(
                    strtoupper(
                        md5(time().rand(1000, 9999))
                    ),
                    $start,
                    $length
                ),
                $split
            )
        );
    }
}
