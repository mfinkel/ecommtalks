<?php

namespace App\Repository;

use App\Entity\HomeConfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeConfig[]    findAll()
 * @method HomeConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeConfig::class);
    }

    public function getHomeConfig(): HomeConfig
    {
        $query = $this->createQueryBuilder('home_config')
            ->select('home_config')
            ->where('home_config.id = :id')
            ->setParameter('id', 1)
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(3600, HomeConfig::CACHE_NAME);

        return $query->getOneOrNullResult();
    }
}
