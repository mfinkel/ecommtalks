<?php

namespace App\Repository;

use App\Entity\RegistrationCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RegistrationCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegistrationCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegistrationCode[]    findAll()
 * @method RegistrationCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistrationCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegistrationCode::class);
    }

    /**
     * @return RegistrationCode[] Returns an array of RegistrationCode objects
     */
    public function getAll()
    {
        return $this->createQueryBuilder('r')
            ->select('r')
            ->orderBy('r.createdAt', Criteria::DESC)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int[] $ids
     */
    public function getExportByIds(array $ids): array
    {
        return $this->createQueryBuilder('r')
            ->select('r')
            ->where('r.id in (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('r.createdAt', Criteria::DESC)
            ->getQuery()
            ->getArrayResult();
    }
}
