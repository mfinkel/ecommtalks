<?php

namespace App\Repository;

use Doctrine\ORM\Query\AST\SimpleArithmeticExpression;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class Rand
{
    /**
     * @var SimpleArithmeticExpression
     */
    private $expression = null;

    public function getSql(SqlWalker $sqlWalker)
    {
        if ($this->expression) {
            return 'RAND('.$this->expression->dispatch($sqlWalker).')';
        }

        return 'RAND()';
    }

    public function parse(Parser $parser)
    {
        $lexer = $parser->getLexer();
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        if (Lexer::T_CLOSE_PARENTHESIS !== $lexer->lookahead['type']) {
            $this->expression = $parser->SimpleArithmeticExpression();
        }

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
