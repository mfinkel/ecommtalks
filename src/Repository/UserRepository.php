<?php

namespace App\Repository;

use App\Entity\RegistrationCode;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

use function get_class;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     * @param UserInterface $user
     * @param string $newEncodedPassword
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }
        
        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }
    
    /**
     * @param string $ipp
     * @param int $currentPage
     * @param array $sortBy
     * @return RegistrationCode[] Returns an array of RegistrationCode objects
     */
    public function exportUsers(string $ipp, int $currentPage, array $sortBy): array
    {
        $sortByFieldName = array_keys($sortBy)[0];
        $query = $this->createQueryBuilder('u')
            ->select('u')
            ->orderBy('u.createdAt', Criteria::DESC)
            ->setMaxResults($ipp)
            ->setFirstResult($ipp * ($currentPage - 1))
            ->orderBy("u.${sortByFieldName}", $sortBy[$sortByFieldName])
            ->getQuery();
        return $query->getResult();
    }
}
