<?php

namespace App\Repository;

use App\Entity\Course;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findAll()
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Course::class);
    }

    /**
     * @return Course[] Returns an array of Course objects
     */
    public function getCoursesList(): iterable
    {
        $query = $this->createQueryBuilder('c')
            ->orderBy('c.orderBy', Criteria::DESC)
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(3600, Course::CACHE_NAME.'_list')
        ;

        return $query->getResult(AbstractQuery::HYDRATE_OBJECT);
    }

    public function getCourseInformation(string $slug)
    {
        $query = $this->createQueryBuilder('c')
            ->select('v', 'c')
            ->leftJoin('c.video', 'v')
            ->andWhere('c.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(3600, Course::CACHE_NAME.$slug);

        return $query->getSingleResult(AbstractQuery::HYDRATE_OBJECT);
    }
}
