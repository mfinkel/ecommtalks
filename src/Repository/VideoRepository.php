<?php

namespace App\Repository;

use App\Entity\Video;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Video|null find($id, $lockMode = null, $lockVersion = null)
 * @method Video|null findOneBy(array $criteria, array $orderBy = null)
 * @method Video[]    findAll()
 * @method Video[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Video::class);
    }
    
    /**
     * @param string $slug
     * @return Video
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getFullVideoInfoBySlug(string $slug): Video
    {
        $query = $this->createQueryBuilder('video')
            ->andWhere('video.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(3600, Video::CACHE_NAME.$slug);

        return $query->getSingleResult(AbstractQuery::HYDRATE_OBJECT);
    }
    
    /**
     * @return int|mixed|string
     */
    public function getHomePageVideo()
    {
        $query = $query = $this->createQueryBuilder('video')
            ->leftJoin('video.speaker', 'speaker')
            ->leftJoin('video.course', 'course')
            ->andWhere('video.isDisplayOnHomePage = :trueForHome')
            ->setParameter('trueForHome', true)
            ->orderBy('video.orderByOnHome', Criteria::DESC)
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(3600, Video::CACHE_NAME.'home_page');

        return $query->getResult(AbstractQuery::HYDRATE_OBJECT);
    }
    
    /**
     * @param Video $video
     * @return int|mixed|string
     */
    public function getRelatedVideo(Video $video)
    {
        $query = $this->createQueryBuilder('video')
            ->select('video', 'speaker', 'course')
            ->leftJoin('video.speaker', 'speaker')
            ->leftJoin('video.course', 'course')
            ->andWhere('video.id != :id')
            ->andWhere('course.id = :courseId')
            ->setParameter('id', $video->getId())
            ->setParameter('courseId', $video->getCourse()->getId())
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(3600, Video::CACHE_NAME.'related_'.$video);

        return $query->getResult(AbstractQuery::HYDRATE_OBJECT);
    }
}
