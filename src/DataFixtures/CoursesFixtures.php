<?php

namespace App\DataFixtures;

use App\Entity\Course;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CoursesFixtures extends Fixture
{
    public function __construct()
    {
    }

    public function load(ObjectManager $manager)
    {
        $list = ['Future section', 'Open Banking Section', 'Fintech section', 'General Section'];
        foreach ($list as $index => $item) {
            $course = new Course();
            $course->setTitle($item)->setIsActive(true);
            $course->setOrderBy($index);
            $manager->persist($course);
        }
        $manager->flush();
    }
}
