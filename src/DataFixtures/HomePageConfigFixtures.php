<?php

namespace App\DataFixtures;

use App\Entity\HomeConfig;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class HomePageConfigFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $config = new HomeConfig();
        $config->setHeaderTitle('<div>Premium Content for Private Community</div>');
        $config->setHeaderDescription('<div>ECOMMTALKS conference presents online Academy - the series of educational and motivational speeches from the IT industry.</div>');
        $config->setLeftColumn('<ul><li>Hot Topics&nbsp;</li><li>Deep Insides&nbsp;</li><li>10+ Hours&nbsp;</li><li>Top Experts&nbsp;</li><li>Global Trends&nbsp;</li><li>Panel Discussions</li></ul>');
        $config->setRightColumn('<ul><li>Future</li><li>Fintech</li><li>Growth</li><li>Regtech</li><li>Ecommerce</li><li>Expansion</li></ul>');
        $manager->persist($config);
        $manager->flush();
    }
}
