<?php

namespace App\Entity;

use App\Repository\HomeConfigRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HomeConfigRepository::class)
 */
class HomeConfig
{
    public const CACHE_NAME = 'home_config_cache';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $headerTitle;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $headerDescription;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $leftColumn;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rightColumn;
    
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    
    /**
     * @return string|null
     */
    public function getHeaderTitle(): ?string
    {
        return $this->headerTitle;
    }
    
    /**
     * @param string $headerTitle
     * @return $this
     */
    public function setHeaderTitle(string $headerTitle): self
    {
        $this->headerTitle = $headerTitle;

        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getHeaderDescription(): ?string
    {
        return $this->headerDescription;
    }
    
    /**
     * @param string $headerDescription
     * @return $this
     */
    public function setHeaderDescription(string $headerDescription): self
    {
        $this->headerDescription = $headerDescription;

        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getLeftColumn(): ?string
    {
        return $this->leftColumn;
    }
    
    /**
     * @param string $leftColumn
     * @return $this
     */
    public function setLeftColumn(string $leftColumn): self
    {
        $this->leftColumn = $leftColumn;

        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getRightColumn(): ?string
    {
        return $this->rightColumn;
    }
    
    /**
     * @param string $rightColumn
     * @return $this
     */
    public function setRightColumn(string $rightColumn): self
    {
        $this->rightColumn = $rightColumn;

        return $this;
    }
}
