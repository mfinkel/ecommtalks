<?php
namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class ContactSpeakerForm
{
    private $type;
    /**
     * @Assert\NotBlank
     * @var string
     */
    private string $message;

    private string $speaker;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
    
    /**
     * @param string $type
     * @return ContactForm
     */
    public function setType(string $type): ContactSpeakerForm
    {
        $this->type = $type;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ContactSpeakerForm
     */
    public function setMessage(string $message): ContactSpeakerForm
    {
        $this->message = $message;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getSpeaker(): string
    {
        return $this->speaker;
    }

    /**
     * @param string $speaker
     * @return ContactSpeakerForm
     */
    public function setSpeaker(string $speaker): ContactSpeakerForm
    {
        $this->speaker = $speaker;

        return $this;
    }
}
