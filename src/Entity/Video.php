<?php

namespace App\Entity;

use App\Repository\VideoRepository;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=VideoRepository::class)
 * @Vich\Uploadable
 */
class Video
{
    public const CACHE_NAME = 'video_cache_';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $description;

    /**
     * @Gedmo\Slug(fields={"id", "title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="video_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string")
     *
     * @var string|null
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int|null
     */
    private $imageSize;

    /**
     * @Orm\Column(type="string", nullable=true)
     */
    private $videoLength;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="video_file", fileNameProperty="videoName", size="videoSize", mimeType="videoType")
     *
     * @var File|null
     */
    private $videoFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    private $videoType;

    /**
     * @ORM\Column(type="string")
     *
     * @var string|null
     */
    private $videoName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int|null
     */
    private $videoSize;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Speaker",
     *     cascade={"persist"},
     *     fetch="LAZY",
     *     cascade={"persist"},
     *     inversedBy="video"
     * )
     * @Orm\JoinColumn(name="speaker_id", referencedColumnName="id")
     */
    private $speaker;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Course",
     *     cascade={"persist"},
     *     fetch="LAZY",
     *     cascade={"persist"},
     *     inversedBy="video"
     * )
     * @Orm\JoinColumn(name="course_id", referencedColumnName="id")
     */
    private $course;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;
    
    /**
     * @ORM\Column(type="smallint")
     */
    private $orderBy;

    /**
     * @ORM\Column(type="smallint")
     */
    private $orderByOnHome;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDisplayOnHomePage;
    
    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->title;
    }
    
    /**
     * Video constructor.
     */
    public function __construct()
    {
        $this->isActive = false;
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
        $this->isDisplayOnHomePage = false;
        $this->orderBy = -1;
    }
    
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    
    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }
    
    /**
     * @param string $slug
     * @return $this
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getVideoLength(): ?string
    {
        return $this->videoLength;
    }
    
    /**
     * @param string|null $videoLength
     * @return $this
     */
    public function setVideoLength(?string $videoLength): self
    {
        $this->videoLength = $videoLength;

        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }
    
    /**
     * @param bool $isActive
     * @return $this
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
    
    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }
    
    /**
     * @param DateTimeInterface $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
    
    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }
    
    /**
     * @param DateTimeInterface $updatedAt
     * @return $this
     */
    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function getIsDisplayOnHomePage(): ?bool
    {
        return $this->isDisplayOnHomePage;
    }
    
    /**
     * @param bool $isDisplayOnHomePage
     * @return $this
     */
    public function setIsDisplayOnHomePage(bool $isDisplayOnHomePage): self
    {
        $this->isDisplayOnHomePage = $isDisplayOnHomePage;

        return $this;
    }
    
    /**
     * @return Speaker|null
     */
    public function getSpeaker(): ?Speaker
    {
        return $this->speaker;
    }
    
    /**
     * @param Speaker|null $speaker
     * @return $this
     */
    public function setSpeaker(?Speaker $speaker): self
    {
        $this->speaker = $speaker;

        return $this;
    }
    
    /**
     * @return Course|null
     */
    public function getCourse(): ?Course
    {
        return $this->course;
    }
    
    /**
     * @param Course|null $course
     * @return $this
     */
    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    /**
     * @param File|null $imageFile
     * @return $this
     */
    public function setImageFile(File $imageFile = null): self
    {
        $this->imageFile = $imageFile;
        if (null !== $imageFile) {
            $this->updatedAt = new DateTimeImmutable();
        }

        return $this;
    }
    
    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }
    
    /**
     * @param string|null $imageName
     * @return $this
     */
    public function setImageName(?string $imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getImageName(): ?string
    {
        return $this->imageName;
    }
    
    /**
     * @param int|null $imageSize
     * @return $this
     */
    public function setImageSize(?int $imageSize): self
    {
        $this->imageSize = $imageSize;

        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }
    
    /**
     * @return File|null
     */
    public function getVideoFile(): ?File
    {
        return $this->videoFile;
    }

    /**
     * @param File|UploadedFile|null $videoFile
     */
    public function setVideoFile(File $videoFile = null): self
    {
        $this->videoFile = $videoFile;
        if (null !== $videoFile) {
            $this->updatedAt = new DateTimeImmutable();
        }

        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getVideoName(): ?string
    {
        return $this->videoName;
    }
    
    /**
     * @param string|null $videoName
     * @return $this
     */
    public function setVideoName(?string $videoName): Video
    {
        $this->videoName = $videoName;

        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getVideoSize(): ?int
    {
        return $this->videoSize;
    }
    
    /**
     * @param int|null $videoSize
     * @return $this
     */
    public function setVideoSize(?int $videoSize): Video
    {
        $this->videoSize = $videoSize;

        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getVideoType(): ?string
    {
        return $this->videoType;
    }
    
    /**
     * @param string|null $videoType
     * @return $this
     */
    public function setVideoType(?string $videoType = null): self
    {
        $this->videoType = $videoType;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }
    
    /**
     * @param mixed $orderBy
     * @return Video
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
        
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getOrderByOnHome()
    {
        return $this->orderByOnHome;
    }
    
    /**
     * @param mixed $orderByOnHome
     * @return $this
     */
    public function setOrderByOnHome($orderByOnHome): self
    {
        $this->orderByOnHome = $orderByOnHome;
        
        return $this;
    }
}
