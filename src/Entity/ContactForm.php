<?php
namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class ContactForm
{
    /**
     * @Assert\NotBlank
     * @var string
     */
    private $fullName;
    /**
     * @Assert\NotBlank
     * @Assert\Email(message = "The email '{{ value }}' is not a valid email.")
     * @var string
     */
    private $email;
    /**
     * @Assert\NotBlank
     * @var string
     */
    private $company;
    /**
     * @Assert\NotBlank
     * @var string
     */
    private $position;
    /**
     * @Assert\NotBlank
     * @var string
     */
    private $type;
    /**
     * @Assert\NotBlank
     * @var string
     */
    private $message;
    
    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }
    
    /**
     * @param string $fullName
     * @return ContactForm
     */
    public function setFullName(string $fullName): ContactForm
    {
        $this->fullName = $fullName;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    
    /**
     * @param string $email
     * @return ContactForm
     */
    public function setEmail(string $email): ContactForm
    {
        $this->email = $email;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }
    
    /**
     * @param string $company
     * @return ContactForm
     */
    public function setCompany(string $company): ContactForm
    {
        $this->company = $company;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }
    
    /**
     * @param string $position
     * @return ContactForm
     */
    public function setPosition(string $position): ContactForm
    {
        $this->position = $position;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
    
    /**
     * @param string $type
     * @return ContactForm
     */
    public function setType(string $type): ContactForm
    {
        $this->type = $type;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
    
    /**
     * @param string $message
     * @return ContactForm
     */
    public function setMessage(string $message): ContactForm
    {
        $this->message = $message;
        
        return $this;
    }
}
