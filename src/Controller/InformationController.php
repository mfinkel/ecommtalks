<?php

namespace App\Controller;

use App\Entity\Information;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class InformationController extends AbstractController
{
    /**
     * @Route("/information/{slug}", name="information")
     * @param string $slug
     * @return Response
     */
    public function indexAction(string $slug): Response
    {
        $info = $this->getDoctrine()->getRepository(Information::class)->getInformationBySlug($slug);
        
        if (!$info) {
            throw new NotFoundHttpException('Page not found');
        }
        
        $templateName = strpos($slug, 'about') !== false ? 'about' : 'index';
        
        return $this->render(
            "information/{$templateName}.html.twig",
            [
                'information' => $info,
            ]
        );
    }
}
