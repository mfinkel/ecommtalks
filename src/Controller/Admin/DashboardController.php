<?php

namespace App\Controller\Admin;

use App\Entity\Course;
use App\Entity\HomeConfig;
use App\Entity\Information;
use App\Entity\RegistrationCode;
use App\Entity\Speaker;
use App\Entity\User;
use App\Entity\Video;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends AbstractDashboardController
{
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            // the name visible to end users
            ->setTitle('Ecommtalks')
            // you can include HTML contents too (e.g. to link to an image)
            ->setTitle('<img src="" alt="">Ecomm<span class="text-small">talks</span>')

            // the path defined in this method is passed to the Twig asset() function
            ->setFaviconPath('favicon.svg')

            // the domain used by default is 'messages'
            ->setTranslationDomain('messages')

            // there's no need to define the "text direction" explicitly because
            // its default value is inferred dynamically from the user locale
            ->setTextDirection('ltr');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToCrud('Registrations codes', 'fa fa-server', RegistrationCode::class),

            MenuItem::linkToCrud('Course', 'fa fa-book', Course::class),

            MenuItem::linkToCrud('Speaker', 'fa fa-graduation-cap', Speaker::class),

            MenuItem::linkToCrud('Video', 'fa fa-video-camera', Video::class),

            MenuItem::linkToCrud('Users', 'fa fa-user', User::class),
            
            MenuItem::linkToCrud('Information', 'fa fa-info-circle', Information::class),
            
            MenuItem::linkToCrud('Home Page Config', 'fa fa-cogs', HomeConfig::class),
        ];
    }
    
    /**
     * @param UserInterface | User $user
     * @return UserMenu
     */
    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)
            ->setName($user->getFullName())
            ->displayUserName(false);
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();
        return $this->redirect($routeBuilder->setController(VideoCrudController::class)->generateUrl());
    }
}
