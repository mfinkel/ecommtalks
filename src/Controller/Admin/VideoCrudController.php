<?php

namespace App\Controller\Admin;

use App\Entity\Video;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class VideoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Video::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        $title = TextField::new('title');
        $description = TextEditorField::new('description');
        $isDisplayOnHomePage = BooleanField::new('isDisplayOnHomePage');

        $coverImage = Field::new('imageFile')->setFormType(VichImageType::class);
        $videoFile = Field::new('videoFile')->setFormType(VichImageType::class);
        $coverImageView = ImageField::new('imageName')->setBasePath('/videoImage');

        $orderBy = NumberField::new('orderBy');
        $orderByOnHome = NumberField::new('orderByOnHome');
        
        $speaker = AssociationField::new('speaker');
        $course = AssociationField::new('course');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$title, $speaker, $course, $coverImageView, $orderBy, $orderByOnHome];
        }

        if (Crud::PAGE_NEW === $pageName) {
            return [
                $title,
                $description,
                $isDisplayOnHomePage,
                $coverImage,
                $videoFile,
                $course,
                $speaker,
                $orderBy,
                $orderByOnHome
            ];
        }

        if (Crud::PAGE_EDIT === $pageName) {
            return [
                $title,
                $description,
                $isDisplayOnHomePage,
                $coverImage,
                $videoFile,
                $course,
                $speaker,
                $orderBy,
                $orderByOnHome
            ];
        }

        return [$title];
    }
}
