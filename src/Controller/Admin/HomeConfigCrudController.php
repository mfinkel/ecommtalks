<?php

namespace App\Controller\Admin;

use App\Entity\HomeConfig;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;

class HomeConfigCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HomeConfig::class;
    }
    
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
            ->remove(Crud::PAGE_DETAIL, Action::DELETE);
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextEditorField::new('headerTitle'),
            TextEditorField::new('headerDescription'),
            TextEditorField::new('leftColumn'),
            TextEditorField::new('rightColumn'),
        ];
    }
}
