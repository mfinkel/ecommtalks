<?php

namespace App\Controller\Admin;

use App\Entity\RegistrationCode;
use App\Service\CsvExporter;
use App\Service\KeyGen;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationCodeCrudController extends AbstractCrudController
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;
    /**
     * @var KeyGen
     */
    private KeyGen $generator;

    /**
     * @var CsvExporter
     */
    private CsvExporter $exporter;

    public function __construct(ValidatorInterface $validator, KeyGen $keyGen, CsvExporter $exporter)
    {
        $this->generator = $keyGen;
        $this->validator = $validator;
        $this->exporter = $exporter;
    }

    public static function getEntityFqcn(): string
    {
        return RegistrationCode::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $testAction = Action::new('export', 'Export list', 'fa fa-file-export')
            ->addCssClass('btn btn-primary')
            ->createAsGlobalAction()
            ->linkToCrudAction('exportAction');

        return $actions
            ->add(Crud::PAGE_INDEX, $testAction)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->remove(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['email', 'registrationCode'])
            ->setDefaultSort(['createdAt' => 'DESC'])
            ->setEntityLabelInSingular('Registration code')
            ->setEntityLabelInPlural('Registration codes')
            ->setPageTitle('index', '%entity_label_plural% listing')
            ->setPaginatorPageSize(30)
            ->setDateTimeFormat('dd.mm.YYYY HH:mm')
            ->setDateFormat('d.m.Y')
            ->setTimeFormat('hh:mm')
            ->overrideTemplates(
                [
                    'crud/index' => 'admin/pages/index.html.twig'
                ]
            );
    }

    public function new(AdminContext $context)
    {
        if (Request::METHOD_POST === $context->getRequest()->getMethod()) {
            $files = $context->getRequest()->files->all();
            $repository = $this->getDoctrine()->getRepository(RegistrationCode::class);
            $rows = null;
            foreach ($files as $file) {
                $content = @file_get_contents($file['file']->getPathname());
                $rows = explode(PHP_EOL, trim($content));
                foreach ($rows as $row) {
                    list($firstName, $lastName, $email) = str_getcsv($row, ',');
                    $email = trim($email);
                    if ('email' === $email || $repository->findOneBy(['email' => $email])) {
                        continue;
                    }

                    $generatedKey = $this->generator->generate();
                    $this->validateEmail($email);
                    $registration = new RegistrationCode();
                    $registration
                        ->setFirstName($firstName)
                        ->setLastName($lastName)
                        ->setEmail($email)
                        ->setRegistrationCode($generatedKey);
                    $this->getDoctrine()->getManager()->persist($registration);
                }
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'success',
                'Add new list of users (' . count($rows) . ')'
            );

            return $this->redirectToRoute(
                'admin',
                [
                    'crudId' => $context->getRequest()->query->get('crudId'),
                    'crudAction' => 'index',
                ]
            );
        }

        return parent::new($context);
    }

    private function validateEmail(string $email): bool
    {
        $emailConstraint = new Assert\Email();
        $emailConstraint->message = 'Invalid email address';
        $errors = $this->validator->validate(
            $email,
            $emailConstraint
        );
        if (0 !== count($errors)) {
            throw new Exception("Email address {$email} is wrong, upload failed, pls fix CSV");
        }

        return true;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('firstName')->onlyOnIndex(),
            TextField::new('lastName')->onlyOnIndex(),
            ImageField::new('file')->onlyOnForms(),
            TextField::new('email')->onlyOnIndex(),
            TextField::new('registrationCode', 'Registration Code')->onlyOnIndex(),
            DateTimeField::new('createdAt', 'Created At')->onlyOnIndex(),
        ];
    }

    /**
     * Return exported to CSV Entity.
     * @param Request $request
     */
    public function exportAction(Request $request): void
    {
        $ids = $request->get('ids');
        $this->exporter->exportRegistrationToCsv(
            $ids
                ? $this->getDoctrine()->getRepository(self::getEntityFqcn())->getExportByIds(
                    json_decode($ids, false, 512, JSON_THROW_ON_ERROR)
                )
                : $this->getDoctrine()->getRepository(self::getEntityFqcn())->getAll()
        );
    }
}
