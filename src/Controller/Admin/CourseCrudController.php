<?php

namespace App\Controller\Admin;

use App\Entity\Course;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CourseCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Course::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IdField::new('id')->onlyOnIndex();
        $email = TextField::new('title');
        $slug = TextField::new('slug')->onlyOnIndex();
        $isActive = BooleanField::new('isActive')->onlyOnIndex();
        $createdAt = DateTimeField::new('createdAt')->onlyOnIndex();

        return [
            $id,
            $email,
            $slug,
            $createdAt,
            $isActive,
            $createdAt,
        ];
    }
}
