<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Service\CsvExporter;
use App\Service\KeyGen;
use Doctrine\Common\Collections\Criteria;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCrudController extends AbstractCrudController
{
    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;
    
    /**
     * @var KeyGen
     */
    private $keyGen;
    
    /**
     * @var CsvExporter
     */
    private $exporter;
    
    const PAGE_SIZE = 15;
    const SORT_BY = 'id';
    const SORT_DIR = Criteria::DESC;

    public function __construct(KeyGen $keyGen, CsvExporter $exporter)
    {
        $this->keyGen = $keyGen;
        $this->exporter = $exporter;
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setDefaultSort([self::SORT_BY => self::SORT_DIR])
            ->setPaginatorPageSize(self::PAGE_SIZE);
        return parent::configureCrud($crud);
    }
    
    public function createEditFormBuilder(
        EntityDto $entityDto,
        KeyValueStore $formOptions,
        AdminContext $context
    ): FormBuilderInterface {
        $formBuilder = parent::createEditFormBuilder($entityDto, $formOptions, $context);

        $this->addEncodePasswordEventListener($formBuilder);

        return $formBuilder;
    }
    
    /**
     * @required
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function setEncoder(UserPasswordEncoderInterface $passwordEncoder): void
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function addEncodePasswordEventListener(FormBuilderInterface $formBuilder)
    {
        $formBuilder->addEventListener(
            FormEvents::SUBMIT,
            function (FormEvent $event) {
                /** @var User $user */
                $user = $event->getData();
                $user->setRegisteredCode($this->keyGen->generate());
                if ($user->getPlainPassword()) {
                    $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPlainPassword()));
                }
            }
        );
    }

    public function createNewFormBuilder(
        EntityDto $entityDto,
        KeyValueStore $formOptions,
        AdminContext $context
    ): FormBuilderInterface {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        $this->addEncodePasswordEventListener($formBuilder);
        return $formBuilder;
    }

    public function configureFields(string $pageName): iterable
    {
        $changePassword = Field::new('plainPassword', 'New password')->onlyOnForms()
            ->setFormType(RepeatedType::class)
            ->setFormTypeOptions(
                [
                    'type' => PasswordType::class,
                    'first_options' => ['label' => 'New password'],
                    'second_options' => ['label' => 'Repeat password'],
                ]
            );
        $newPassword = Field::new('plainPassword', 'New password')
            ->onlyOnForms();

        $id = IdField::new('id')->hideOnForm();
        $UserDetailsTab = FormField::addPanel('User Details');
        $fullName = TextField::new('fullName');
        $firstName = TextField::new('firstName');
        $lastName = TextField::new('lastName');
        $registeredCode = TextField::new('registeredCode');
        $facebookId = TextField::new('facebookId');
        $googleId = TextField::new('googleId');
        $isActive = BooleanField::new('isActive');

        $separator = FormField::addPanel();
        $createdAt = DateTimeField::new('createdAt')->onlyOnDetail();

        $userRolesTab = FormField::addPanel('User Role')
            ->setIcon('fa fa-user')
            ->addCssClass('optional')
            ->setHelp('Select user role');

        $roles = ChoiceField::new('roles')
            ->setChoices(
                [
                    'Customer' => 'ROLE_USER',
                    'Admin' => 'ROLE_ADMIN',
                ]
            )
            ->allowMultipleChoices()
            ->setLabel('User Role');
        $email = TextField::new('email');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $firstName, $lastName, $email, $registeredCode, $isActive, $facebookId, $googleId, $createdAt];
        }

        if (Crud::PAGE_NEW === $pageName) {
            return [
                $UserDetailsTab,
                $firstName,
                $lastName,
                $separator,
                $userRolesTab,
                $email,
                $roles,
                $newPassword,
            ];
        }

        if (Crud::PAGE_EDIT === $pageName) {
            return [
                $UserDetailsTab,
                $firstName,
                $lastName,
                $separator,
                $userRolesTab,
                $email,
                $roles,
                $changePassword,
            ];
        }

        return [$id, $fullName, $email, $createdAt];
    }

    /**
     * @param Actions $actions
     * @return Actions
     */
    public function configureActions(Actions $actions): Actions
    {
        $testAction = Action::new('export', 'Export current page', 'fa fa-file-export')
            ->addCssClass('btn btn-primary')
            ->createAsGlobalAction()
            ->linkToCrudAction('exportAction');
        
        return $actions
            ->add(Crud::PAGE_INDEX, $testAction);
    }
    
    
    /**
     * Return exported to CSV Entity.
     * @param Request $request
     */
    public function exportAction(Request $request): void
    {
        $currentPage = $request->query->get('page') ?? 1;
        $sort = $request->query->get('sort');
        if (!$sort) {
            $sort = [self::SORT_BY => self::SORT_DIR];
        }
        $this->exporter->exportUsersToCsv(
            $this->getDoctrine()->getRepository(self::getEntityFqcn())->exportUsers(self::PAGE_SIZE, $currentPage, $sort)
        );
    }
}
