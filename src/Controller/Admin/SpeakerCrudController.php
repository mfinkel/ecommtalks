<?php

namespace App\Controller\Admin;

use App\Entity\Speaker;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SpeakerCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Speaker::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ImageField::new('profilePic')
                ->setBasePath('/speakerImage')
                ->onlyOnIndex(),

            IdField::new('id')->onlyOnIndex(),
            TextField::new('fullName')->onlyOnIndex(),
            TextField::new('firstName')->onlyOnForms(),
            TextField::new('lastName')->onlyOnForms(),
            EmailField::new('email')->onlyOnForms(),
            TextField::new('company'),
            TextField::new('position'),
            BooleanField::new('isActive'),
            TextEditorField::new('bio')->onlyOnForms(),
            Field::new('imageFile')->setFormType(VichImageType::class)->onlyOnForms(),
            NumberField::new('orderBy')
        ];
    }
}
