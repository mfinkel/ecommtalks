<?php

namespace App\Controller;

use App\Security\GoogleAuthenticator;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\Provider\FacebookClient;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\FacebookUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class FacebookController extends AbstractController
{
    private $authenticator;
    private $guardHandler;

    public function __construct(
        GoogleAuthenticator $authenticator,
        GuardAuthenticatorHandler $guardHandler
    ) {
        $this->authenticator = $authenticator;
        $this->guardHandler = $guardHandler;
    }

    /**
     * Link to this controller to start the "connect" process.
     *
     * @Route("/connect/facebook", name="connect_facebook_start")
     * @param ClientRegistry $clientRegistry
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('facebook') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect(
                [
                    'public_profile',
                    'email',
                ],
                []
            );
    }

    /**
     * After going to Facebook, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml.
     *
     * @Route("/connect/facebook/check", name="connect_facebook_check")
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     * @return Response|null
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
        /** @var FacebookClient $client */
        $client = $clientRegistry->getClient('facebook');

        try {
            /** @var FacebookUser|UserInterface $user */
            $accessToken = $client->getAccessToken();
            $user = $client->fetchUserFromToken($accessToken);

            return $this->guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $this->authenticator,
                'main'
            );
        } catch (IdentityProviderException $e) {
            throw new AccessDeniedException('Access denied');
        }
    }
}
