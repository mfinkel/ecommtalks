<?php

namespace App\Controller;

use App\Entity\Course;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CoursesController extends AbstractController
{
    /**
     * @Route("/courses", name="index_courses")
     */
    public function index()
    {
        $courses = $this->getDoctrine()->getRepository(Course::class)->getCoursesList();

        return $this->render('courses/index.html.twig', [
            'courses' => $courses,
        ]);
    }

    /**
     * @Route("/courses/{slug}", name="view_courses")
     *
     * @return Response
     */
    public function viewCourseAction(string $slug)
    {
        $course = $this->getDoctrine()->getRepository(Course::class)->getCourseInformation($slug);

        if (!$course) {
            throw new NotFoundHttpException('Course not found');
        }

        return $this->render(
            'courses/course.html.twig',
            [
                'course' => $course,
                'controller_name' => 'CoursesController',
            ]
        );
    }
}
