<?php

namespace App\Controller;

use App\Entity\HomeConfig;
use App\Entity\RegistrationCode;
use App\Entity\User;
use App\Entity\Video;
use App\Form\RegistrationFormType;
use App\Security\AppAuthenticator;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{
    /**
     * @var MailerService
     */
    private $mailerService;

    public function __construct(MailerService $mailerService)
    {
        $this->mailerService = $mailerService;
    }
    
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler $guardHandler
     * @param AppAuthenticator $authenticator
     * @return Response
     * @throws TransportExceptionInterface
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        AppAuthenticator $authenticator
    ): Response {
        $user = new User();
        $email = $request->query->get('email');
        $registeredCode = $request->query->get('code');

        $user->setEmail($email ? $email : '');
        $user->setRegisteredCode($registeredCode ? $registeredCode : '');
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        $homePageConfig = $this->getDoctrine()->getRepository(HomeConfig::class)->find(1);
        $homePageVideo = $this->getDoctrine()->getRepository(Video::class)->getHomePageVideo();
        shuffle($homePageVideo);

        if ($form->isSubmitted() && $form->isValid()) {
            $code = $this->getDoctrine()->getRepository(RegistrationCode::class)->findOneBy(
                [
                    'email' => $user->getEmail(),
                    'registrationCode' => $user->getRegisteredCode(),
                ]
            );
            if (!$code) {
                $this->addFlash('error', 'app.register.code_failure');

                return $this->redirect($this->generateUrl('app_register'));
            }
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setFirstName($code->getFirstName());
            $user->setLastName($code->getLastName());
            $user->setIsActive(true);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email

            $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );

            $this->mailerService->sendConfirmationEmail($user);

            return $this->redirect(
                $this->generateUrl('registration_success')
            );
        }

        return $this->render(
            'registration/register.html.twig',
            [
                'registrationForm' => $form->createView(),
                'homePageVideo' => array_slice($homePageVideo, 0, $this->getParameter('homepage_video_limit')),
                'homePageConfig' => $homePageConfig
            ]
        );
    }

    /**
     * @Route("/registration/success", name="registration_success")
     */
    public function registrationSuccess()
    {
        return $this->render(
            'registration/success.html.twig',
            [
            ]
        );
    }
}
