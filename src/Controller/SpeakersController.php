<?php

namespace App\Controller;

use App\Entity\Speaker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SpeakersController extends AbstractController
{
    /**
     * @Route("/speakers", name="index_speakers")
     */
    public function index()
    {
        $speakers = $this->getDoctrine()->getRepository(Speaker::class)->findBy(
            ['isActive' => true],
            ['orderBy' => 'DESC']
        );
        
        return $this->render(
            'speakers/index.html.twig',
            [
                'speakers' => $speakers,
            ]
        );
    }
    
    public function setProfilePic(?string $name)
    {
        $this->profilePic = $name;
        
        return $this;
    }
}
