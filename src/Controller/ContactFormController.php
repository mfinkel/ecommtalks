<?php

namespace App\Controller;

use App\Entity\Speaker;
use App\Form\ContactFormType;
use App\Form\ContactSpeakerFormType;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ContactFormController extends AbstractController
{
    /**
     * @var MailerService
     */
    private $mailerService;
    
    public function __construct(MailerService $mailerService)
    {
        $this->mailerService = $mailerService;
    }
    
    /**
     * @Route("/contact/form", name="contact_form", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws TransportExceptionInterface
     */
    public function index(Request $request, ValidatorInterface $validator): Response
    {
        $form = $this->createForm(ContactFormType::class, null, ['csrf_protection' => false]);
       // var_dump($request->request->all());
        $form->submit($request->request->all());

        $errors = $validator->validate($form);
        if ($form->isValid()) {
            $type = $form->get('type')->getData();
            $sendTo = $this->getParameter("${type}_form_email");
            $sendToSubject = $this->getParameter("${type}_form_subject");
            $fromName = $form->get('fullName')->getData();
            $fromEmail = $form->get('email')->getData();
            $fromCompany = $form->get('company')->getData();
            $fromPosition = $form->get('position')->getData();
            $fromMessage = $form->get('message')->getData();
            $this->mailerService->sendContactFormEmail(
                $sendTo,
                $sendToSubject,
                $fromName,
                $fromEmail,
                $fromCompany,
                $fromPosition,
                $fromMessage
            );
            return $this->json(['status' => 'success'])->setStatusCode(200);
        }
        
        return $this->json($errors)->setStatusCode(400);
    }

    /**
     * @Route("/contact/speaker", name="speaker_form", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws TransportExceptionInterface
     */
    public function speaker(Request $request, ValidatorInterface $validator): Response
    {
        $form = $this->createForm(ContactSpeakerFormType::class, null, ['csrf_protection' => false]);

        $form->submit($request->request->all());

        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        $errors = $validator->validate($form);
        if ($form->isValid()) {
            $type = $form->get('type')->getData();
            $speaker = $form->get('speaker')->getData();
            $sendTo = $this->getParameter("${type}_form_email");
            $subject = "Contact speaker: ".$speaker;
            $fromName = $user->getFirstName().' '.$user->getLastName();
            $fromEmail = $user->getEmail();
            $fromMessage = $form->get('message')->getData();
            $this->mailerService->sendContactSpeakerFormEmail(
                $speaker,
                $sendTo,
                $subject,
                $fromName,
                $fromEmail,
                $fromMessage
            );
            return $this->json(['status' => 'success'])->setStatusCode(200);
        }

        return $this->json($errors)->setStatusCode(400);
    }
}
