<?php

namespace App\Controller;

use App\Entity\HomeConfig;
use App\Entity\Video;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('index');
        }
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        $homePageVideo = $this->getDoctrine()->getRepository(Video::class)->getHomePageVideo();
        $homePageConfig = $this->getDoctrine()->getRepository(HomeConfig::class)->getHomeConfig();

        return $this->render('login/index.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'homePageVideo' => array_slice($homePageVideo, 0, $this->getParameter('homepage_video_limit')),
            'homePageConfig' => $homePageConfig,
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
