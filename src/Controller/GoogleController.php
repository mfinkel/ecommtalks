<?php

namespace App\Controller;

use App\Security\GoogleAuthenticator;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\Provider\GoogleClient;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\GoogleUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class GoogleController extends AbstractController
{
    private $authenticator;
    private $guardHandler;

    public function __construct(
        GoogleAuthenticator $authenticator,
        GuardAuthenticatorHandler $guardHandler
    ) {
        $this->authenticator = $authenticator;
        $this->guardHandler = $guardHandler;
    }

    /**
     * Link to this controller to start the "connect" process.
     *
     * @Route("/connect/google", name="connect_google_start")
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('google')
            ->redirect(
                ['email'],
                []
            );
    }

    /**
     * @Route("/connect/google/check", name="connect_google_check")
     * @param ClientRegistry $clientRegistry
     * @return RedirectResponse|Response|null
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
        /** @var googleClient $client */
        $client = $clientRegistry->getClient('google');

        try {
            /** @var GoogleUser|UserInterface $user */
            $accessToken = $client->getAccessToken();
            $user = $client->fetchUserFromToken($accessToken);

            return $this->guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $this->authenticator,
                'main'
            );
        } catch (IdentityProviderException $e) {
            return new RedirectResponse('Login with oAuth failed, reason Unknown', Response::HTTP_FOUND);
        }
    }
}
