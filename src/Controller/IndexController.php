<?php

namespace App\Controller;

use App\Entity\HomeConfig;
use App\Entity\Video;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $homePageVideo = $this->getDoctrine()->getRepository(Video::class)->getHomePageVideo();
        $homePageConfig = $this->getDoctrine()->getRepository(HomeConfig::class)->getHomeConfig();

        return $this->render(
            'index/index.html.twig',
            [
                'homePageVideo' => array_slice($homePageVideo, 0, $this->getParameter('homepage_video_limit')),
                'homePageConfig' => $homePageConfig,
            ]
        );
    }
}
