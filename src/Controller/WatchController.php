<?php

namespace App\Controller;

use App\Entity\Video;
use App\Service\VideoStream;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class WatchController extends AbstractController
{
    /**
     * @Route("/watch/{slug}", name="watch")
     *
     * @return Response
     */
    public function index(string $slug)
    {
        try {
            $video = $this->getDoctrine()->getRepository(Video::class)->getFullVideoInfoBySlug($slug);
        } catch (NoResultException $exception) {
            throw new NotFoundHttpException('Cannot find requested video');
        }
        $relatedVideo = $this->getDoctrine()->getRepository(Video::class)->getRelatedVideo($video);
        shuffle($relatedVideo);

        return $this->render(
            'watch/base.html.twig',
            [
                'controller_name' => 'WatchController',
                'video_id' => $slug,
                'video' => $video,
                'relatedVideo' => array_slice($relatedVideo, 0, 3),
            ]
        );
    }

    /**
     * @Route("/stream/{slug}", name="stream_video")
     *
     * @return Response
     */
    public function streamAction(string $slug)
    {
        $video = $this->getDoctrine()->getRepository(Video::class)->getFullVideoInfoBySlug($slug);

        if (!$video) {
            throw new NotFoundHttpException();
        }

        $videosDir = $this->getParameter('video_file_storage');
        if (file_exists($filePath = $videosDir.'/'.$video->getVideoName())) {
            $stream = new VideoStream($filePath);

            return new StreamedResponse(
                function () use ($stream) {
                    $stream->start();
                }
            );
        }

        return new Response('', Response::HTTP_NOT_FOUND);
    }
}
