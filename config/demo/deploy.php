<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class() extends DefaultDeployer {
    public function configure()
    {
        return $this->getConfigBuilder()
            ->symfonyEnvironment('prod')
            ->server('moris@185.185.126.32')
            ->deployDir('/var/www/html/ecommtalks.moris.org.ua')
            ->repositoryUrl('git@bitbucket.org:mfinkel/ecommtalks.git')
            ->repositoryBranch('master')
            ->sharedFilesAndDirs(
                [
                    'upload/',
                ]
            )
            ->keepReleases(3);
    }

    public function beforePreparing()
    {
        $this->runRemote('sudo chown -R moris: /var/www/html/ecommtalks.moris.org.ua/');
        $this->log('Copy env  file to deploy');
        $this->runRemote('cp {{ deploy_dir }}/repo/.env {{ project_dir }}/.env');
        $this->log('Dump autoload for any reasons');
        $this->runRemote('export APP_ENV=prod; cd {{ project_dir }} && composer dump-autoload');
    }

    public function beforeStartingDeploy()
    {
    }

    public function beforeFinishingDeploy()
    {
        $this->log('Install nodeJS dependencies');
        $this->runRemote('yarn');
        $this->log('Run encore prod build');
        $this->runRemote('yarn build');
        $this->log('Run Doctrine migrations');
        $this->runRemote('./bin/console d:m:m -n');

        $this->log('Create symlink for uploaded images');
        $this->runRemote('ln -s "$(pwd)/upload/videoImage" "$(pwd)/public/"');
        $this->runRemote('ln -s "$(pwd)/upload/speakerImage" "$(pwd)/public/"');

        $this->runRemote('sudo chown -R www-data: upload/');
        $this->runRemote('sudo chown -R www-data: var/');
    }

    public function beforeCancelingDeploy()
    {
    }
};
